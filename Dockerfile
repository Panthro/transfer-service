FROM openjdk:11 as builder

EXPOSE 8080

WORKDIR /app

COPY . /app

RUN ./mvnw -ntp clean package


FROM openjdk:11-slim as runner


COPY --from=builder /app/target/*-exec.jar /app.jar

CMD ["java","-jar","/app.jar"]

