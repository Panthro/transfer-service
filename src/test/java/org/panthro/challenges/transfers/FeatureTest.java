package org.panthro.challenges.transfers;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber.json"},
        features = {"classpath:feature/"},
        glue = "org.panthro.challenges.transfers.steps",
        strict = true
)
public class FeatureTest {

}
