package org.panthro.challenges.transfers.healthcheck;


import org.junit.Test;
import org.panthro.challenges.transfers.service.InMemoryTransferService;

import java.math.BigDecimal;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class ServiceAccountsHealthCheckTest {


    @Test
    public void healthyWhenTransferServiceHasAccounts() {
        final ServiceAccountsHealthCheck healthCheck = new ServiceAccountsHealthCheck(new InMemoryTransferService(Collections.singletonMap("one", BigDecimal.ZERO)));

        assertThat(healthCheck.check().isHealthy()).isTrue();
    }

    @Test
    public void unhealthyWhenTransferServiceIsEmpty() {
        final ServiceAccountsHealthCheck healthCheck = new ServiceAccountsHealthCheck(new InMemoryTransferService(Collections.emptyMap()));

        assertThat(healthCheck.check().isHealthy()).isFalse();
    }
}