package org.panthro.challenges.transfers.steps;

import org.panthro.challenges.transfers.error.TransferException;
import org.panthro.challenges.transfers.model.Transfer;
import org.panthro.challenges.transfers.service.TransferService;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
@Setter
public class TransferWorld {

    private String debtorAccountId;

    private String creditorAccountId;

    private BigDecimal transferAmount;

    private Transfer resultingTransfer;

    private TransferException transferException;

    private TransferService transferService;

}
