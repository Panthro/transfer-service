package org.panthro.challenges.transfers.steps;

import org.panthro.challenges.transfers.dto.TransferRequest;
import org.panthro.challenges.transfers.error.AccountNotFoundException;
import org.panthro.challenges.transfers.error.InsufficientFundsException;
import org.panthro.challenges.transfers.error.InvalidAmountException;
import org.panthro.challenges.transfers.error.SameAccountException;
import org.panthro.challenges.transfers.error.TransferException;
import org.panthro.challenges.transfers.model.Transfer;
import org.panthro.challenges.transfers.service.InMemoryTransferService;
import org.panthro.challenges.transfers.service.TransferService;

import java.math.BigDecimal;
import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferSteps {

    private final TransferWorld world;

    public TransferSteps(TransferWorld world) {
        this.world = world;
    }

    @Given("^the following initial accounts:$")
    public void the_following_initial_accounts(DataTable data) {
        final Map<String, BigDecimal> initialAccounts = data.asMap(String.class, BigDecimal.class);
        TransferService transferService = new InMemoryTransferService(initialAccounts);
        world.transferService(transferService);
    }


    @Given("^debtor account is (.+)$")
    public void debtor_account_is_account(String accountId) {
        world.debtorAccountId(accountId);
    }

    @Given("^creditor account is (.+)$")
    public void creditor_account_is_account(String accountId) {
        world.creditorAccountId(accountId);
    }

    @When("^transfer (.+) from debtor to creditor$")
    public void transfer_from_debtor_to_creditor(BigDecimal amount) {
        world.transferAmount(amount);
        try {
            final TransferRequest request = new TransferRequest(world.debtorAccountId(), world.creditorAccountId(), world.transferAmount());
            world.resultingTransfer(getTransferService().transfer(request));
        } catch (TransferException e) {
            world.transferException(e);
        }
    }

    @Then("^debtor balance is (.+)$")
    public void debtor_balance_is(BigDecimal debtorBalance) {
        assertThat(getTransferService().getBalance(world.debtorAccountId())).isEqualTo(debtorBalance);
    }

    @Then("^creditor balance is (.+)$")
    public void creditor_balance_is(BigDecimal creditorBalance) {
        assertThat(getTransferService().getBalance(world.creditorAccountId())).isEqualTo(creditorBalance);
    }

    private TransferService getTransferService() {
        return world.transferService();
    }

    @Then("^insufficient funds detected$")
    public void insufficientFundsDetected() {
        assertThat(world.transferException()).isInstanceOf(InsufficientFundsException.class);
    }

    @Then("^invalid amount detected$")
    public void invalidAmountDetected() {
        assertThat(world.transferException()).isInstanceOf(InvalidAmountException.class);
    }

    @Then("^account not found detected$")
    public void accountNotFoundDetected() {
        assertThat(world.transferException()).isInstanceOf(AccountNotFoundException.class);
    }


    @Then("^same account detected$")
    public void invalidAccountDetected() {
        assertThat(world.transferException()).isInstanceOf(SameAccountException.class);

    }


    @And("^matching transfer is returned$")
    public void matchingTransferIsReturned() {
        final Transfer resultingTransfer = world.resultingTransfer();
        assertThat(resultingTransfer).isNotNull();
        assertThat(resultingTransfer.getId()).isNotEmpty();
        assertThat(resultingTransfer.getDebtorAccount()).isEqualTo(world.debtorAccountId());
        assertThat(resultingTransfer.getCreditorAccount()).isEqualTo(world.creditorAccountId());
        assertThat(resultingTransfer.getAmount()).isEqualTo(world.transferAmount());
    }

    @And("^matching transfer is present in transfer log$")
    public void matchingTransferIsPresentInTransferLog() {
        assertThat(getTransferService().getTransfers()).anyMatch(this::matchWorldTransfer);
    }

    @And("^no matching transfer is present in transfer log$")
    public void noMatchingTransferIsPresentInTransferLog() {
        assertThat(getTransferService().getTransfers()).noneMatch(this::matchWorldTransfer);
    }

    private boolean matchWorldTransfer(Transfer transfer) {
        return transfer.getCreditorAccount().equals(world.creditorAccountId())
                && transfer.getDebtorAccount().equals(world.debtorAccountId())
                && transfer.getAmount().compareTo(world.transferAmount()) == 0;
    }

}
