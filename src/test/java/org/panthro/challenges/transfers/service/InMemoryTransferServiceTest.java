package org.panthro.challenges.transfers.service;

import org.junit.Before;
import org.junit.Test;
import org.panthro.challenges.transfers.dto.TransferRequest;
import org.panthro.challenges.transfers.error.AccountNotFoundException;
import org.panthro.challenges.transfers.error.InsufficientFundsException;
import org.panthro.challenges.transfers.error.InvalidAmountException;
import org.panthro.challenges.transfers.error.SameAccountException;
import org.panthro.challenges.transfers.error.TransferException;
import org.panthro.challenges.transfers.model.Transfer;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryTransferServiceTest {

    private static final Map<String, BigDecimal> INITIAL_DATA = new HashMap<String, BigDecimal>() {{
        put("0", new BigDecimal(0));
        put("1", new BigDecimal(100));
        put("2", new BigDecimal(200));
        put("3", new BigDecimal(300));
        put("4", new BigDecimal(4000));
        put("5", new BigDecimal(5000));
        put("6", new BigDecimal(6000));
        put("10", new BigDecimal(10000));
    }};

    private InMemoryTransferService transferService;

    @Before
    public void setUp() {
        transferService = new InMemoryTransferService(INITIAL_DATA);
    }

    @Test
    public void getBalance() throws AccountNotFoundException {
        assertThat(transferService.getBalance("0")).isEqualTo(BigDecimal.ZERO);
        assertThat(transferService.getBalance("10")).isEqualTo(new BigDecimal(10000));
    }

    @Test
    public void validTransfer() throws TransferException {
        //Given
        String creditorAccount = "0";
        String debtorAccount = "10";
        BigDecimal initialCreditorAccountBalance = transferService.getBalance(creditorAccount);
        BigDecimal initialDebtorAccountBalance = transferService.getBalance(debtorAccount);
        BigDecimal transferAmount = new BigDecimal(1000);

        //WHEN
        final Transfer transfer = transferService.transfer(new TransferRequest(debtorAccount, creditorAccount, transferAmount));

        //THEN

        assertThat(transfer).isNotNull();
        assertThat(transfer.getId()).isNotNull();
        assertThat(transfer.getInstant()).isBefore(Instant.now());
        assertThat(transfer.getAmount()).isEqualTo(transferAmount);
        assertThat(transfer.getCreditorAccount()).isEqualTo(creditorAccount);
        assertThat(transfer.getDebtorAccount()).isEqualTo(debtorAccount);
        assertThat(transfer.getCreditorInitialBalance()).isEqualTo(initialCreditorAccountBalance);
        assertThat(transfer.getDebtorInitialBalance()).isEqualTo(initialDebtorAccountBalance);
        assertThat(transfer.getCreditorFinalBalance()).isEqualTo(initialCreditorAccountBalance.add(transferAmount));
        assertThat(transfer.getDebtorFinalBalance()).isEqualTo(initialDebtorAccountBalance.subtract(transferAmount));

        assertThat(transferService.getBalance(debtorAccount)).isEqualTo(initialDebtorAccountBalance.subtract(transferAmount));
        assertThat(transferService.getBalance(creditorAccount)).isEqualTo(initialCreditorAccountBalance.add(transferAmount));

        assertThat(transferService.getTransfers()).size().isEqualTo(1);
    }

    @Test(expected = InvalidAmountException.class)
    public void invalidAmountNegative() {
        //Given
        String creditorAccount = "0";
        String debtorAccount = "10";
        BigDecimal transferAmount = new BigDecimal(-10);

        //WHEN
        transferService.transfer(new TransferRequest(debtorAccount, creditorAccount, transferAmount));
    }

    @Test(expected = InvalidAmountException.class)
    public void invalidAmountZero() {
        //Given
        String creditorAccount = "0";
        String debtorAccount = "10";
        BigDecimal transferAmount = new BigDecimal(0);

        //WHEN
        transferService.transfer(new TransferRequest(debtorAccount, creditorAccount, transferAmount));
    }

    @Test(expected = SameAccountException.class)
    public void sameAccounts() {
        //Given
        String creditorAccount = "0";
        String debtorAccount = "0";
        BigDecimal transferAmount = new BigDecimal(1);

        //WHEN
        transferService.transfer(new TransferRequest(debtorAccount, creditorAccount, transferAmount));
    }

    @Test(expected = AccountNotFoundException.class)
    public void invalidDebtorAccount() {
        //Given
        String creditorAccount = "0";
        String debtorAccount = "not_account";
        BigDecimal transferAmount = new BigDecimal(1);

        //WHEN
        transferService.transfer(new TransferRequest(debtorAccount, creditorAccount, transferAmount));
    }

    @Test(expected = AccountNotFoundException.class)
    public void invalidCreditorAccount() {
        //Given
        String creditorAccount = "not_account";
        String debtorAccount = "0";
        BigDecimal transferAmount = new BigDecimal(1);

        //WHEN
        transferService.transfer(new TransferRequest(debtorAccount, creditorAccount, transferAmount));
    }

    @Test(expected = InsufficientFundsException.class)
    public void insufficientFunds() {
        //Given
        String creditorAccount = "1";
        String debtorAccount = "0";
        BigDecimal transferAmount = new BigDecimal(10);

        //WHEN
        transferService.transfer(new TransferRequest(debtorAccount, creditorAccount, transferAmount));
    }

    @Test
    public void getAccountBalance() {
        assertThat(transferService.getBalance("0")).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    public void getBalances() {
        assertThat(transferService.getBalances()).containsExactlyEntriesOf(INITIAL_DATA);
    }

    @Test
    public void getTransfersIsEmpty() {
        assertThat(transferService.getTransfers()).isEmpty();
    }

}