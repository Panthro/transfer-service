package org.panthro.challenges.transfers.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.panthro.challenges.transfers.dto.TransferRequest;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import static java.util.stream.Collectors.collectingAndThen;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class ConcurrentInMemoryTransferServiceTest {
    private final TestSetup testSetup;
    private final ExecutorService executor;
    private final InMemoryTransferService transferService;
    private final List<TransferRequest> transfers;


    public ConcurrentInMemoryTransferServiceTest(String testFile) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        final InputStream testSetupStream = this.getClass().getResourceAsStream(testFile);
        this.testSetup = objectMapper.readValue(testSetupStream, TestSetup.class);
        transfers = loadRequestsFromFile(testFile.replaceAll("\\.ya?ml", ".csv"));
        executor = Executors.newFixedThreadPool(10);
        transferService = new InMemoryTransferService(testSetup.initialAccounts);
    }

    private static ArrayList<TransferRequest> shuffling(ArrayList<TransferRequest> list) {
        Collections.shuffle(list);
        return list;
    }

    private static TransferRequest lineToRequest(String line) {
        final String[] split = line.split(",");
        return new TransferRequest(split[0].trim(), split[1].trim(), new BigDecimal(split[2].trim()));
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<String> data() {
        Reflections reflections = new Reflections("concurrent", new ResourcesScanner());
        return reflections.getResources(Pattern.compile(".*\\.yml"))
                .stream()
                .map(s -> "/" + s)
                .collect(Collectors.toSet());
    }

    private List<TransferRequest> loadRequestsFromFile(String requestsFile) throws IOException {
        try (InputStream is = this.getClass().getResourceAsStream(requestsFile)) {

            final Stream<TransferRequest> requestStream = new BufferedReader(new InputStreamReader(is))
                    .lines()
                    .map(ConcurrentInMemoryTransferServiceTest::lineToRequest);
            if (testSetup.randomizeRequests) {
                return requestStream
                        // this is to make sure the list is randomly ordered so the transfer execution order is random
                        .collect(collectingAndThen(
                                Collectors.toCollection(ArrayList::new),
                                ConcurrentInMemoryTransferServiceTest::shuffling
                        ));
            } else {
                return requestStream.collect(Collectors.toList());
            }

        }
    }

    private Runnable wrap(TransferRequest transferRequest) {
        return () -> transferService.transfer(transferRequest);
    }

    @Test
    public void executeTransfers() throws InterruptedException {
        transfers.stream()
                .map(this::wrap)
                .forEach(executor::submit);
        executor.shutdown();
        executor.awaitTermination(testSetup.timeout, TimeUnit.MILLISECONDS);
        assertThat(transferService.getBalances()).containsAllEntriesOf(testSetup.finalBalances);
    }

    @Getter
    @Setter
    private static final class TestSetup {
        @NonNull
        private Boolean randomizeRequests = true;
        @NonNull
        private Long timeout = 1000L;
        private Map<String, BigDecimal> initialAccounts;
        private Map<String, BigDecimal> finalBalances;
    }


}