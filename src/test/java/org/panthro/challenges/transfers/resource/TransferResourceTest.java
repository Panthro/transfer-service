package org.panthro.challenges.transfers.resource;

import org.apache.http.HttpHeaders;
import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.panthro.challenges.transfers.TransferServiceApplication;
import org.panthro.challenges.transfers.dto.TransferRequest;

import java.math.BigDecimal;

import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;


public class TransferResourceTest {

    private static final String ACCOUNT_WITH_BALANCE = "account-1";
    private static final String ACCOUNT_EMPTY = "account-2";
    private static final String INVALID_ACCOUNT = "account-nope";

    private static TransferServiceApplication application;

    @BeforeClass
    public static void setUp() throws Exception {
        application = new TransferServiceApplication();
        application.run("server", "test-accounts.yml");
        baseURI = "http://localhost:5555/transfer";
    }

    @Test
    public void isHealthy() {
        get("http://localhost:6666/healthcheck").then().statusCode(200);
    }

    @Test
    public void getBalances() {
        get("/balances")
                .then().statusCode(200)
                .body(ACCOUNT_WITH_BALANCE, greaterThan(0))
                .body(ACCOUNT_EMPTY, greaterThanOrEqualTo(0));
    }

    @Test
    public void getValidAccountBalance() {
        get("/balances/" + ACCOUNT_WITH_BALANCE)
                .then().statusCode(200)
                .body(ACCOUNT_WITH_BALANCE, greaterThan(0));
    }

    @Test
    public void getInvalidAccountBalanceShouldReturn404() {
        get("/balances/" + INVALID_ACCOUNT)
                .then().statusCode(404);
    }

    @Test
    public void validTransfer() {
        given()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .body(new TransferRequest(ACCOUNT_WITH_BALANCE, ACCOUNT_EMPTY, BigDecimal.TEN))
                .post()
                .then()
                .statusCode(200)
                .body("id", Matchers.not(empty()))
                .body("debtorAccount", is(ACCOUNT_WITH_BALANCE))
                .body("creditorAccount", is(ACCOUNT_EMPTY))
                .body("amount", notNullValue())
                .body("creditorInitialBalance", notNullValue())
                .body("debtorFinalBalance", notNullValue())
                .body("creditorInitialBalance", notNullValue())
                .body("creditorFinalBalance", notNullValue())
        ;
    }

    @Test
    public void insufficientFunds() {
        given()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .body(new TransferRequest(ACCOUNT_EMPTY, ACCOUNT_WITH_BALANCE, new BigDecimal(1000)))
                .post()
                .then()
                .statusCode(409);
    }

    @Test
    public void invalidAmount() {
        given()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .body(new TransferRequest(ACCOUNT_EMPTY, ACCOUNT_WITH_BALANCE, new BigDecimal(-1)))
                .post()
                .then()
                .statusCode(422);
    }

    @Test
    public void sameAccount() {
        given()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .body(new TransferRequest(ACCOUNT_WITH_BALANCE, ACCOUNT_WITH_BALANCE, new BigDecimal(1)))
                .post()
                .then()
                .statusCode(409);
    }


}