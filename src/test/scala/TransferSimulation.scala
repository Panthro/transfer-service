import org.panthro.challenges.transfers.TransferServiceApplication

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._


class TransferSimulation extends Simulation {

  before {
    new TransferServiceApplication()
      .run("server", "test-accounts.yml")
  }
  val httpProtocol = http
    .baseUrl(s"http://localhost:5555")
    .acceptHeader("application/json")
    .contentTypeHeader("application/json")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")


  val getBalances = scenario("Get Balances")
    .exec(http("GET /balances")
      .get("/transfer/balances")
      .check(status.is(200)))


  val transferAccount1To2 = scenario("Transfer from account 1 to 2")
    .exec(http("POST /transfer")
      .post("/transfer")
      .body(StringBody(_ => s"""{ "amount":2,"debtorAccount": "account-1","creditorAccount": "account-2" }""")).asJson
      .check(status.is(200)))


  val transferAccount6To1 = scenario("Transfer from account 6 to 1")
    .exec(http("POST /transfer")
      .post("/transfer")
      .body(StringBody(_ => s"""{ "amount":2,"debtorAccount": "account-6","creditorAccount": "account-1" }""")).asJson
      .check(status.is(200)))

  val transferAccount2To3 = scenario("Transfer from account 2 to 4")
    .exec(http("POST /transfer")
      .post("/transfer")
      .body(StringBody(_ => s"""{ "amount":1,"debtorAccount": "account-2","creditorAccount": "account-3" }""")).asJson
      .check(status.is(200)))


  val duration = 30

  setUp(


    // 50 users getting balance at the same time
    getBalances.inject(rampUsersPerSec(1) to 100 during duration),
    // constantly transfer small amount
    transferAccount1To2.inject(constantUsersPerSec(10) during (duration seconds)),
    transferAccount6To1.inject(constantUsersPerSec(10) during (duration seconds)),
//    // every second transfer amount, this check locks
    transferAccount2To3.inject(constantUsersPerSec(1) during (duration seconds)),
  ).protocols(httpProtocol)


}

