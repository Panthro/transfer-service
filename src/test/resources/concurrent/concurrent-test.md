# Concurrent tests

More concurrent tests can be created to test new test cases and scenarios
The idea is that if a scenario is found to be problematic, it can be dropped 
here from the transaction log and it will be tested in a multi thread scenario


# Adding new test cases

1. Create a file `sample.yml` with the structure:

```yaml
# If requests are executed in order or not
randomizeRequests: false #default
# You can change timeout of requests execution
timeout: 5000 # default
initialAccounts:
  account-1: 1000
  account-2: 1000
  account-3: 1000
  account-4: 1000
finalBalances:
  account-1: 983.73
  account-2: 825.95
  account-3: 1090.32
  account-4: 1100
```

2. Then create a file `sample.csv` (must match file name in step 1) with the transactions:

Example:
```csv
account-1,account-2,99
account-2,account-1,99.9
account-3,account-1,57.65
account-1,account-3,33.99
account-2,account-3,25.5
account-2,account-3,12
account-1,account-2,34
account-2,account-1,23
account-1,account-2,20
account-1,account-3,33
account-3,account-1,12.5
account-2,account-1,10.67
account-2,account-3,55.98
account-2,account-4,100
```


__NOTE:__ you can use [this spreadsheet](transaction-book-generator.xlsx) to help you generate transactions

# Running

The scenarios are ran using [Junit4 Parametrized Tests ](https://github.com/junit-team/junit4/wiki/parameterized-tests), 
just run the `ConcurrentInMemoryTransferServiceTest` class  