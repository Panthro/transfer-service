Feature: Transfer money between accounts

  Background: Initial accounts
    Given the following initial accounts:
      | account-1 | 1000 |
      | account-2 | 0    |
    And debtor account is account-1
    And creditor account is account-2

  Scenario Outline: Transfer between accounts
    When transfer <amount> from debtor to creditor
    Then debtor balance is <finalDebtorBalance>
    And creditor balance is <finalCreditorBalance>
    And matching transfer is returned
    And matching transfer is present in transfer log

    Examples:
      | amount | finalDebtorBalance | finalCreditorBalance |
      | 100    | 900                | 100                  |
      | 300    | 700                | 300                  |
      | 1000   | 0                  | 1000                 |
      | 999.99 | 0.01               | 999.99               |

  Scenario: Insufficient funds
    When transfer 2000 from debtor to creditor
    Then insufficient funds detected
    And debtor balance is 1000
    And creditor balance is 0
    And no matching transfer is present in transfer log


  Scenario Outline: Invalid amount
    When transfer <amount> from debtor to creditor
    Then invalid amount detected
    And debtor balance is 1000
    And creditor balance is 0
    And no matching transfer is present in transfer log

    Examples:
      | amount |
      | -1     |
      | -99999 |
      | 0      |
      | -9     |

  Scenario: Account not found
    And creditor account is account-nope
    When transfer 1 from debtor to creditor
    Then account not found detected
    And debtor balance is 1000
    And no matching transfer is present in transfer log


  Scenario: Same account
    And creditor account is account-1
    When transfer 1 from debtor to creditor
    Then same account detected
    And debtor balance is 1000
    And no matching transfer is present in transfer log


