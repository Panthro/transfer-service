package org.panthro.challenges.transfers.provider;

import org.panthro.challenges.transfers.error.AccountNotFoundException;
import org.panthro.challenges.transfers.error.InsufficientFundsException;
import org.panthro.challenges.transfers.error.InvalidAmountException;
import org.panthro.challenges.transfers.error.SameAccountException;
import org.panthro.challenges.transfers.error.TransferException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Maps {@link TransferException} to a {@link Response} so jax-rs knows what to answer
 * when an exception occurs.
 */
@Slf4j
public class TransferExceptionMapper implements ExceptionMapper<TransferException> {

    private static final Map<Class<? extends TransferException>, Integer> statusMap = new HashMap<>();

    static {
        statusMap.put(AccountNotFoundException.class, 404);
        statusMap.put(InvalidAmountException.class, 422);
        statusMap.put(InsufficientFundsException.class, 409);
        statusMap.put(SameAccountException.class, 409);
    }

    @Override
    public Response toResponse(TransferException exception) {
        final Integer status = Optional.ofNullable(statusMap.get(exception.getClass())).orElse(500);
        return Response
                .status(status)
                .entity(new ErrorResponse(exception.getMessage(), status))
                .build();
    }

    @Data
    private static final class ErrorResponse {
        private final String message;
        private final int status;
    }
}
