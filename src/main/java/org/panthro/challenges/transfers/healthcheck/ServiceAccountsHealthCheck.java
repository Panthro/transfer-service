package org.panthro.challenges.transfers.healthcheck;

import com.codahale.metrics.health.HealthCheck;

import org.panthro.challenges.transfers.service.TransferService;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Dummy health check that only checks the service has initial accounts
 */
public class ServiceAccountsHealthCheck extends HealthCheck {

    private final TransferService transferService;

    public ServiceAccountsHealthCheck(TransferService transferService) {
        this.transferService = transferService;
    }

    @Override
    protected Result check() {
        final Map<String, BigDecimal> balances = this.transferService.getBalances();
        if (balances.isEmpty()) {
            return Result.unhealthy("Transfer service has no accounts");
        }
        return Result.healthy("Transfer service has %d accounts", balances.size());
    }
}
