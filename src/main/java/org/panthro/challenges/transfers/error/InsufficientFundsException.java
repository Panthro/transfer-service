package org.panthro.challenges.transfers.error;

public class InsufficientFundsException extends TransferException {
    public InsufficientFundsException(String account) {
        super("Insufficient funds on account :" + account);
    }
}
