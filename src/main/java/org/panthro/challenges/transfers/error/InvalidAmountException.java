package org.panthro.challenges.transfers.error;

import java.math.BigDecimal;

public class InvalidAmountException extends TransferException {

    public InvalidAmountException(BigDecimal amount) {
        super("The amount was invalid: " + amount);
    }
}
