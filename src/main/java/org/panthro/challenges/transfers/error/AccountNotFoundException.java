package org.panthro.challenges.transfers.error;

public class AccountNotFoundException extends TransferException {
    public AccountNotFoundException(String account) {
        super("Account not found: " + account);
    }
}
