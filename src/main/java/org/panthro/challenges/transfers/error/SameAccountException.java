package org.panthro.challenges.transfers.error;

public class SameAccountException extends TransferException {

    public SameAccountException(String account) {
        super(String.format("Creditor account and debtor account must be different: [%s]", account));
    }
}
