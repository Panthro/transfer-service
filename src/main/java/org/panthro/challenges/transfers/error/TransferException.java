package org.panthro.challenges.transfers.error;

/**
 * Exception thrown when a transfer cannot be executed
 */
public abstract class TransferException extends RuntimeException {

    TransferException(String message) {
        super(message);
    }

}
