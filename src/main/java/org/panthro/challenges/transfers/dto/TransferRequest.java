package org.panthro.challenges.transfers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@JsonDeserialize(builder = TransferRequest.TransferRequestBuilder.class)
@Value
@AllArgsConstructor
@Builder
@Schema(name = "Transfer request", example = "{\"debtorAccount\":\"account-1\",\"creditorAccount\":\"account-2\",\"amount\":100}")
public final class TransferRequest {

    @NotNull
    @NotEmpty
    @JsonProperty
    @Schema(required = true, description = "The debtor account")
    private final String debtorAccount;
    @NotNull
    @NotEmpty
    @JsonProperty
    @Schema(required = true, description = "The creditor account")
    private final String creditorAccount;
    @NotNull
    @Min(value = 0)
    @JsonProperty
    @Schema(required = true, description = "The transfer amount")
    private final BigDecimal amount;

}
