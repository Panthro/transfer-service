package org.panthro.challenges.transfers;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Map;

import io.dropwizard.Configuration;
import io.swagger.v3.oas.models.info.Info;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class TransferServiceConfiguration extends Configuration {

    @JsonProperty
    private Map<String, BigDecimal> initialAccounts;

    @JsonProperty
    private Info apiInfo;

}
