package org.panthro.challenges.transfers.resource;


import com.codahale.metrics.annotation.Timed;

import org.panthro.challenges.transfers.dto.TransferRequest;
import org.panthro.challenges.transfers.model.Transfer;
import org.panthro.challenges.transfers.service.TransferService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * The transfer resource. All transfer apis are defined here
 */
@Path("/transfer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransferResource {

    private final TransferService transferService;

    public TransferResource(TransferService transferService) {
        this.transferService = transferService;
    }


    // TODO This is actually just a utility for the challenge as the specification was widely open
    @Path("/balances")
    @GET
    @Timed
    @Operation(description = "Get balances all known accounts")
    public
    @ApiResponse(responseCode = "200", content = @Content(examples = @ExampleObject("[{\"account-1\":1000},{\"account-2\":200} ]")))
    Map<String, BigDecimal> balances() {
        return transferService.getBalances();
    }

    // TODO This is actually just a utility for the challenge as the specification was widely open
    @Path("/balances/{accountId}")
    @GET
    @Timed
    @Operation(description = "Get balance of an account", responses = {
            @ApiResponse(responseCode = "404", description = "When account is not found"),
    })
    public
    @ApiResponse(responseCode = "200", content = @Content(examples = @ExampleObject("{\"account-1\":1000}")))
    Map<String, BigDecimal> accountBalance(
            @Parameter(example = "account-1")
            @NotEmpty @PathParam("accountId") String accountId) {
        return Collections.singletonMap(accountId, transferService.getBalance(accountId));
    }

    @POST
    @Timed
    @Operation(description = "Transfers money from debtor account to creditor account", responses = {
            @ApiResponse(responseCode = "404", description = "When one of the accounts was not found"),
            @ApiResponse(responseCode = "409", description = "When debtor and creditor accounts are the same or the debtor account does not have funds"),
            @ApiResponse(responseCode = "422", description = "When the amount is invalid"),
    })
    public
    @ApiResponse(responseCode = "200", description = "The transfer executed")
    Transfer transfer(@Valid TransferRequest transferRequest) {
        return transferService.transfer(transferRequest);
    }

}
