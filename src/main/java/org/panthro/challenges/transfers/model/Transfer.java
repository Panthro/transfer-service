package org.panthro.challenges.transfers.model;

import java.math.BigDecimal;
import java.time.Instant;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "debtorAccount", "creditorAccount", "amount"})
@Builder
public class Transfer {

    @NonNull
    private final String id;
    @NonNull
    private final Instant instant;
    @NonNull
    private final String debtorAccount;
    @NonNull
    private final String creditorAccount;
    @NonNull
    private final BigDecimal amount;

    //TODO assuming it is for internal usage from another service,
    // so this information is leaked to the response, if that is not the case
    // just annotation the properties below with @JsonIgnore
    @NonNull
    private final BigDecimal debtorInitialBalance;
    @NonNull
    private final BigDecimal debtorFinalBalance;
    @NonNull
    private final BigDecimal creditorInitialBalance;
    @NonNull
    private final BigDecimal creditorFinalBalance;

}
