package org.panthro.challenges.transfers.service;

import org.panthro.challenges.transfers.dto.TransferRequest;
import org.panthro.challenges.transfers.error.AccountNotFoundException;
import org.panthro.challenges.transfers.error.TransferException;
import org.panthro.challenges.transfers.model.Transfer;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Transfer service is design to transfer money between accounts and optionally
 * hold get the balances.
 */
public interface TransferService {
    /**
     * Get the balance of a given account
     *
     * @param accountId the account
     * @return a {@link BigDecimal} that holds the balance
     * @throws AccountNotFoundException if the account is unknown
     */
    BigDecimal getBalance(String accountId) throws AccountNotFoundException;

    /**
     * Transfer money from debtor account to the creditor account.
     * <p>Implementations should validate all aspects of transfer, from inputs to balances</p>
     *
     * @param transferRequest The details of the transfer
     * @throws TransferException In case any validation fails
     */
    Transfer transfer(TransferRequest transferRequest) throws TransferException;

    /**
     * Optionally store and retrieve all previous transfers
     * TODO: pagination
     *
     * @return a list with all previous transfers
     */
    List<Transfer> getTransfers();

    /**
     * Optionally get balances for all accounts known to the service
     *
     * @return a map with Account id: Balance
     */
    Map<String, BigDecimal> getBalances();
}
