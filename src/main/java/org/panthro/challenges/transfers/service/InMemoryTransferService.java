package org.panthro.challenges.transfers.service;

import org.panthro.challenges.transfers.dto.TransferRequest;
import org.panthro.challenges.transfers.error.AccountNotFoundException;
import org.panthro.challenges.transfers.error.InsufficientFundsException;
import org.panthro.challenges.transfers.error.InvalidAmountException;
import org.panthro.challenges.transfers.error.SameAccountException;
import org.panthro.challenges.transfers.error.TransferException;
import org.panthro.challenges.transfers.model.Transfer;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.BiFunction;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static java.util.UUID.randomUUID;

/**
 * An In Memory implementation of the transfer service
 * The transfers and balance operations are thread-safe as the implementation
 * is backed by a {@link ConcurrentHashMap} and only atomic operations are used
 * <br />
 * For the Transaction log a {@link ConcurrentLinkedQueue} is used in order to preserve order
 * in a multi thread environment as it guarantees ordering on write operations,
 * and perform better than a {@link java.util.concurrent.CopyOnWriteArrayList}
 */
@Slf4j
public final class InMemoryTransferService implements TransferService {

    // this should be an external repository, a database, but I'm choosing KISS over the S in SOLID
    private final ConcurrentHashMap<String, BigDecimal> accountBalances;
    // this is like lightweight transaction log, again KISS, only succeeded transactions
    private final ConcurrentLinkedQueue<Transfer> transfers;

    /**
     * Constructor that initialize the accounts with the initial balances
     *
     * @param initialBalances the initial account balances
     */
    public InMemoryTransferService(@NonNull Map<String, BigDecimal> initialBalances) {
        if (initialBalances.isEmpty()) {
            log.warn("Transfer service initialized without any account");
        } else {
            log.info("Transfer service initialized with {} accounts", initialBalances.size());
        }
        this.accountBalances = new ConcurrentHashMap<>(initialBalances);
        this.transfers = new ConcurrentLinkedQueue<>();
    }

    @Override
    public BigDecimal getBalance(String accountId) throws AccountNotFoundException {
        return Optional.ofNullable(accountBalances.get(accountId))
                .orElseThrow(() -> new AccountNotFoundException(accountId));
    }

    @Override
    public Transfer transfer(final TransferRequest transferRequest) throws TransferException {
        log.debug("Transfer request {}", transferRequest);
        final String debtorAccount = transferRequest.getDebtorAccount();
        final String creditorAccount = transferRequest.getCreditorAccount();
        final BigDecimal amount = transferRequest.getAmount();
        checkAmount(amount);
        checkAccounts(debtorAccount, creditorAccount);
        log.info("All checks passed, executing transfer request {}", transferRequest);


        final Transfer.TransferBuilder transferBuilder = Transfer.builder()
                .id(randomUUID().toString())
                .creditorAccount(creditorAccount)
                .debtorAccount(debtorAccount)
                .amount(amount);
        accountBalances.computeIfPresent(debtorAccount, debitAmount(amount, transferBuilder));
        accountBalances.computeIfPresent(creditorAccount, creditAmount(amount, transferBuilder));
        final Transfer transfer = transferBuilder.instant(Instant.now()).build();
        transfers.add(transfer);
        log.info("Transfer executed {}", transfer.getId());
        log.debug("Transfer finished {}", transfer);
        return transfer;
    }

    private BiFunction<String, BigDecimal, BigDecimal> creditAmount(BigDecimal amount, Transfer.TransferBuilder transferBuilder) {
        return (account, balance) -> {
            log.debug("Crediting {} to account {}", amount, account);
            final BigDecimal creditorFinalBalance = balance.add(amount);
            transferBuilder.creditorInitialBalance(balance).creditorFinalBalance(creditorFinalBalance);
            return creditorFinalBalance;
        };
    }

    private BiFunction<String, BigDecimal, BigDecimal> debitAmount(BigDecimal amount, Transfer.TransferBuilder transferBuilder) {
        return (account, balance) -> {
            if (balance.compareTo(amount) < 0) {
                log.warn("Invalid funds for account [{}] - [{}]", account, amount);
                throw new InsufficientFundsException(account);
            }
            log.debug("Withdrawing {} from account {}", amount, account);
            final BigDecimal debtorFinalBalance = balance.subtract(amount);
            transferBuilder.debtorInitialBalance(balance).debtorFinalBalance(debtorFinalBalance);
            return debtorFinalBalance;
        };
    }

    private void checkAccounts(String debtorAccount, String creditorAccount) {
        checkAccount(debtorAccount);
        checkAccount(creditorAccount);
        if (Objects.equals(debtorAccount, creditorAccount)) {
            log.debug("Debtor account and creditor account are the same {}", debtorAccount);
            throw new SameAccountException(debtorAccount);
        }

    }

    @Override
    public List<Transfer> getTransfers() {
        // S(O)LID
        return Collections.unmodifiableList(new ArrayList<>(transfers));
    }

    @Override
    public Map<String, BigDecimal> getBalances() {
        // S(O)LID
        return Collections.unmodifiableMap(accountBalances);
    }

    private void checkAmount(BigDecimal amount) throws InvalidAmountException {
        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            log.warn("Invalid amount for transfer [{}]", amount);
            throw new InvalidAmountException(amount);
        }
    }

    private void checkAccount(String account) throws AccountNotFoundException {
        if (accountBalances.get(account) == null) {
            log.warn("Account not found [{}]", account);
            throw new AccountNotFoundException(account);
        }
    }
}
