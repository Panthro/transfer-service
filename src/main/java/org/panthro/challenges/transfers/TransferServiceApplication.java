package org.panthro.challenges.transfers;

import com.google.common.collect.ImmutableSet;

import org.panthro.challenges.transfers.healthcheck.ServiceAccountsHealthCheck;
import org.panthro.challenges.transfers.provider.TransferExceptionMapper;
import org.panthro.challenges.transfers.resource.TransferResource;
import org.panthro.challenges.transfers.service.InMemoryTransferService;
import org.panthro.challenges.transfers.service.TransferService;

import io.dropwizard.Application;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;

public class TransferServiceApplication extends Application<TransferServiceConfiguration> {


    private static final String[] DEFAULT_ARGS = new String[]{"server", "application.yml"};

    public static void main(final String[] args) throws Exception {
        new TransferServiceApplication().run(args.length > 0 ? args : DEFAULT_ARGS);
    }

    @Override
    public String getName() {
        return "InMemoryTransferService";
    }

    @Override
    public void initialize(final Bootstrap<TransferServiceConfiguration> bootstrap) {
        // this loads the configuration from src/main/resources instead of current working directory
        bootstrap.setConfigurationSourceProvider(new ResourceConfigurationSourceProvider());

    }

    @Override
    public void run(final TransferServiceConfiguration configuration, final Environment environment) {
        /*
            As it is a simple setup no IoC was needed,
            if it starts to get more complex,
            it is recommended to use an IoC framework, like google guice.
        */
        final TransferService transferService = new InMemoryTransferService(configuration.getInitialAccounts());
        environment.healthChecks().register("initialAccounts", new ServiceAccountsHealthCheck(transferService));
        environment.jersey().register(new TransferResource(transferService));
        environment.jersey().register(new TransferExceptionMapper());
        environment.jersey().register(new OpenApiResource().openApiConfiguration(buildSwaggerConfiguration(configuration)));

    }

    private SwaggerConfiguration buildSwaggerConfiguration(TransferServiceConfiguration configuration) {
        return new SwaggerConfiguration()
                .openAPI(new OpenAPI().info(configuration.getApiInfo()))
                .prettyPrint(true)
                .resourcePackages(ImmutableSet.of(this.getClass().getPackage().getName()));
    }


}
