# TransferService

![](https://img.shields.io/badge/dropwizard-blue) 
![](https://img.shields.io/badge/java->=8-green)  

## Features

```gherkin
Feature: Transfer money between accounts

  Background: Initial accounts
    Given the following initial accounts:
      | account-1 | 1000 |
      | account-2 | 0    |
    And debtor account is account-1
    And creditor account is account-2

  Scenario Outline: Transfer between accounts
    When transfer <amount> from debtor to creditor
    Then debtor balance is <finalDebtorBalance>
    And creditor balance is <finalCreditorBalance>
    And matching transfer is returned
    And matching transfer is present in transfer log

    Examples:
      | amount | finalDebtorBalance | finalCreditorBalance |
      | 100    | 900                | 100                  |
      | 300    | 700                | 300                  |
      | 1000   | 0                  | 1000                 |
      | 999.99 | 0.01               | 999.99               |

  Scenario: Insufficient funds
    When transfer 2000 from debtor to creditor
    Then insufficient funds detected
    And debtor balance is 1000
    And creditor balance is 0
    And no matching transfer is present in transfer log


  Scenario Outline: Invalid amount
    When transfer <amount> from debtor to creditor
    Then invalid amount detected
    And debtor balance is 1000
    And creditor balance is 0
    And no matching transfer is present in transfer log

    Examples:
      | amount |
      | -1     |
      | -99999 |
      | 0      |
      | -9     |

  Scenario: Account not found
    And creditor account is account-nope
    When transfer 1 from debtor to creditor
    Then account not found detected
    And debtor balance is 1000
    And no matching transfer is present in transfer log


  Scenario: Same account
    And creditor account is account-1
    When transfer 1 from debtor to creditor
    Then same account detected
    And debtor balance is 1000
    And no matching transfer is present in transfer log

```

## How to start the TransferService application

1. Run `./mvnw clean install` to build your application
1. Start application with `java -jar target/transfer-service-1.0-SNAPSHOT-exec.jar`
1. Application configuration can be seen (and updated) in `src/main/resources/application.yml` 
1. To check that your application is running enter url `http://localhost:8082` (changed from 8081 as mcafee holds 8081)

## OpenApi specification

Visit <http://localhost:8080/openapi.json> (or yaml), this can be loaded in [swagger-ui](https://petstore.swagger.io/) so you can play with the api 

## Health Check

To see applications health visit <http://localhost:8082/healthcheck>


## Initial Data
The application starts with initial data provided in the `src/main/resources/application.yml`:

```yaml
initialAccounts:
  # account id : initial Balance
  account-1: 1000000
  account-2: 0
  account-3: 2000000
  account-4: 0
  account-5: 3000000
  account-6: 0
```

# Endpoints
__Note:__ If opening with Intellij Idea the requests presented in this readme are executable

### Creating transactions

```http request
POST http://localhost:8080/transfer
Content-Type: application/json

{
"debtorAccount":"account-1",
"creditorAccount":"account-2",
"amount":10
}
```

### Get account balance
---

__NOTE:__ This is a utility method as specifications were widely open

Single account:
```http request
GET http://localhost:8080/transfer/balances/account-1
Content-Type: application/json
```

All accounts
```http request
GET http://localhost:8080/transfer/balances
Content-Type: application/json
```

Get all balances

Tests
---
TDD was using for development so all features are tested.
BDD was used for development as well, so features were developed after defined in .feature file

- Unit test ![](https://img.shields.io/badge/using-junit4-green)
- Integration Test ![](https://img.shields.io/badge/using-restasured-green)
- BDD tests ![](https://img.shields.io/badge/using-cucumber-green)
- Performance Tests ![](https://img.shields.io/badge/using-gatling-green) 
- [Concurrency Tests](src/test/resources/concurrent/concurrent-test.md) 

Assumptions
---

- Accounts would be available from another service/repository,
that is why there is no _create account_ endpoint and there is the _initialAccounts_ config
- All accounts have the same currency, so no currency (and thus exchange) support is given
- A transfer log must be stored for audit (not in memory of course)
- Overdraft is now allowed (`InsufficientFundsException`)
- Only positive non 0 amounts are allowed `InvalidAmountException`
- Transfer from and to the same account is now allowed `SameAccountException`

Known Issues
---

- Error formats are not 100% consistent 
    (when validation fails in resource vs when inside transaction service)
- If all endpoints are enabled in swagger, 
    dropwizard exposed endpoints cannot be used with swagger ui 



